import {Dimensions} from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  window: {
    width,
    height,
  },
  isSmallDevice: width < 375,
};

export const SUBTITLES = {
  key: 'startTime',
  convert: true,
  textKey: 'content',
  data: [
    {
      duration: 0,
      content:
        'This is really a two-hour presentation I give to high school students,',
      startOfParagraph: null,
      startTime: '00:00:11.851',
    },
    {
      duration: 0,
      content: 'cut down to three minutes.',
      startOfParagraph: null,
      startTime: '00:00:15.165',
    },
    {
      duration: 0,
      content: 'And it all started one day on a plane, on my way to TED,',
      startOfParagraph: null,
      startTime: '00:00:16.440',
    },
    {
      duration: 0,
      content: 'seven years ago.',
      startOfParagraph: null,
      startTime: '00:00:19.105',
    },
    {
      duration: 0,
      content:
        'And in the seat next to me was a high school student, a teenager,',
      startOfParagraph: null,
      startTime: '00:00:20.423',
    },
    {
      duration: 0,
      content: 'and she came from a really poor family.',
      startOfParagraph: null,
      startTime: '00:00:24.820',
    },
    {
      duration: 0,
      content: 'And she wanted to make something of her life,',
      startOfParagraph: null,
      startTime: '00:00:27.303',
    },
    {
      duration: 0,
      content: 'and she asked me a simple little question.',
      startOfParagraph: null,
      startTime: '00:00:29.820',
    },
    {
      duration: 0,
      content: 'She said, "What leads to success?"',
      startOfParagraph: null,
      startTime: '00:00:31.883',
    },
    {
      duration: 0,
      content: 'And I felt really badly,',
      startOfParagraph: null,
      startTime: '00:00:33.852',
    },
    {
      duration: 0,
      content: "because I couldn't give her a good answer.",
      startOfParagraph: null,
      startTime: '00:00:35.255',
    },
    {
      duration: 0,
      content: 'So I get off the plane, and I come to TED.',
      startOfParagraph: null,
      startTime: '00:00:37.740',
    },
    {
      duration: 0,
      content:
        "And I think, jeez, I'm in the middle of a room of successful people!",
      startOfParagraph: null,
      startTime: '00:00:39.820',
    },
    {
      duration: 0,
      content: "So why don't I ask them what helped them succeed,",
      startOfParagraph: null,
      startTime: '00:00:43.574',
    },
    {
      duration: 0,
      content: 'and pass it on to kids?',
      startOfParagraph: null,
      startTime: '00:00:46.209',
    },
    {
      duration: 0,
      content: 'So here we are, seven years, 500 interviews later,',
      startOfParagraph: null,
      startTime: '00:00:48.637',
    },
    {
      duration: 0,
      content: "and I'm going to tell you what really leads to success",
      startOfParagraph: null,
      startTime: '00:00:52.270',
    },
    {
      duration: 0,
      content: 'and makes TEDsters tick.',
      startOfParagraph: null,
      startTime: '00:00:55.234',
    },
    {
      duration: 0,
      content: 'And the first thing is passion.',
      startOfParagraph: null,
      startTime: '00:00:57.187',
    },
    {
      duration: 0,
      content: 'Freeman Thomas says, "I\'m driven by my passion."',
      startOfParagraph: null,
      startTime: '00:00:59.607',
    },
    {
      duration: 0,
      content: "TEDsters do it for love; they don't do it for money.",
      startOfParagraph: null,
      startTime: '00:01:02.583',
    },
    {
      duration: 0,
      content: 'Carol Coletta says, "I would pay someone to do what I do."',
      startOfParagraph: null,
      startTime: '00:01:05.067',
    },
    {
      duration: 0,
      content: 'And the interesting thing is:',
      startOfParagraph: null,
      startTime: '00:01:08.575',
    },
    {
      duration: 0,
      content: 'if you do it for love, the money comes anyway.',
      startOfParagraph: null,
      startTime: '00:01:10.010',
    },
    {
      duration: 0,
      content: 'Work! Rupert Murdoch said to me, "It\'s all hard work.',
      startOfParagraph: null,
      startTime: '00:01:12.686',
    },
    {
      duration: 0,
      content: 'Nothing comes easily. But I have a lot of fun."',
      startOfParagraph: null,
      startTime: '00:01:15.820',
    },
    {
      duration: 0,
      content: 'Did he say fun? Rupert? Yes!',
      startOfParagraph: null,
      startTime: '00:01:18.927',
    },
    {
      duration: 0,
      content: '(Laughter)',
      startOfParagraph: null,
      startTime: '00:01:21.821',
    },
    {
      duration: 0,
      content: 'TEDsters do have fun working. And they work hard.',
      startOfParagraph: null,
      startTime: '00:01:23.321',
    },
    {
      duration: 0,
      content: "I figured, they're not workaholics. They're workafrolics.",
      startOfParagraph: null,
      startTime: '00:01:26.091',
    },
    {
      duration: 0,
      content: '(Laughter)',
      startOfParagraph: null,
      startTime: '00:01:29.068',
    },
    {
      duration: 0,
      content: 'Good!',
      startOfParagraph: null,
      startTime: '00:01:30.682',
    },
    {
      duration: 0,
      content: '(Applause)',
      startOfParagraph: null,
      startTime: '00:01:31.763',
    },
    {
      duration: 0,
      content:
        'Alex Garden says, "To be successful, put your nose down in something',
      startOfParagraph: null,
      startTime: '00:01:32.788',
    },
    {
      duration: 0,
      content: 'and get damn good at it."',
      startOfParagraph: null,
      startTime: '00:01:36.158',
    },
    {
      duration: 0,
      content: "There's no magic; it's practice, practice, practice.",
      startOfParagraph: null,
      startTime: '00:01:37.428',
    },
    {
      duration: 0,
      content: "And it's focus.",
      startOfParagraph: null,
      startTime: '00:01:40.294',
    },
    {
      duration: 0,
      content: 'Norman Jewison said to me,',
      startOfParagraph: null,
      startTime: '00:01:41.337',
    },
    {
      duration: 0,
      content:
        '"I think it all has to do with focusing yourself on one thing."',
      startOfParagraph: null,
      startTime: '00:01:43.095',
    },
    {
      duration: 0,
      content: 'And push!',
      startOfParagraph: null,
      startTime: '00:01:46.593',
    },
    {
      duration: 0,
      content: 'David Gallo says, "Push yourself.',
      startOfParagraph: null,
      startTime: '00:01:48.055',
    },
    {
      duration: 0,
      content: 'Physically, mentally, you\'ve got to push, push, push."',
      startOfParagraph: null,
      startTime: '00:01:50.067',
    },
    {
      duration: 0,
      content: "You've got to push through shyness and self-doubt.",
      startOfParagraph: null,
      startTime: '00:01:52.733',
    },
    {
      duration: 0,
      content: 'Goldie Hawn says, "I always had self-doubts.',
      startOfParagraph: null,
      startTime: '00:01:55.368',
    },
    {
      duration: 0,
      content: "I wasn't good enough; I wasn't smart enough.",
      startOfParagraph: null,
      startTime: '00:01:57.820',
    },
    {
      duration: 0,
      content: "I didn't think I'd make it.\"",
      startOfParagraph: null,
      startTime: '00:01:59.940',
    },
    {
      duration: 0,
      content: "Now it's not always easy to push yourself,",
      startOfParagraph: null,
      startTime: '00:02:02.084',
    },
    {
      duration: 0,
      content: "and that's why they invented mothers.",
      startOfParagraph: null,
      startTime: '00:02:04.147',
    },
    {
      duration: 0,
      content: '(Laughter)',
      startOfParagraph: null,
      startTime: '00:02:06.259',
    },
    {
      duration: 0,
      content: '(Applause)',
      startOfParagraph: null,
      startTime: '00:02:07.259',
    },
    {
      duration: 0,
      content: 'Frank Gehry said to me,',
      startOfParagraph: null,
      startTime: '00:02:08.820',
    },
    {
      duration: 0,
      content: '"My mother pushed me."',
      startOfParagraph: null,
      startTime: '00:02:11.820',
    },
    {
      duration: 0,
      content: '(Laughter)',
      startOfParagraph: null,
      startTime: '00:02:13.214',
    },
    {
      duration: 0,
      content: 'Serve!',
      startOfParagraph: null,
      startTime: '00:02:14.452',
    },
    {
      duration: 0,
      content:
        'Sherwin Nuland says, "It was a privilege to serve as a doctor."',
      startOfParagraph: null,
      startTime: '00:02:16.247',
    },
    {
      duration: 0,
      content: 'A lot of kids want to be millionaires.',
      startOfParagraph: null,
      startTime: '00:02:19.913',
    },
    {
      duration: 0,
      content: 'The first thing I say is:',
      startOfParagraph: null,
      startTime: '00:02:22.047',
    },
    {
      duration: 0,
      content: '"OK, well you can\'t serve yourself;',
      startOfParagraph: null,
      startTime: '00:02:23.321',
    },
    {
      duration: 0,
      content: "you've got to serve others something of value.",
      startOfParagraph: null,
      startTime: '00:02:25.247',
    },
    {
      duration: 0,
      content: 'Because that\'s the way people really get rich."',
      startOfParagraph: null,
      startTime: '00:02:27.508',
    },
    {
      duration: 0,
      content: 'Ideas!',
      startOfParagraph: null,
      startTime: '00:02:30.894',
    },
    {
      duration: 0,
      content: 'TEDster Bill Gates says, "I had an idea:',
      startOfParagraph: null,
      startTime: '00:02:31.943',
    },
    {
      duration: 0,
      content: 'founding the first micro-computer software company."',
      startOfParagraph: null,
      startTime: '00:02:34.820',
    },
    {
      duration: 0,
      content: "I'd say it was a pretty good idea.",
      startOfParagraph: null,
      startTime: '00:02:37.820',
    },
    {
      duration: 0,
      content: "And there's no magic to creativity in coming up with ideas --",
      startOfParagraph: null,
      startTime: '00:02:39.820',
    },
    {
      duration: 0,
      content: "it's just doing some very simple things.",
      startOfParagraph: null,
      startTime: '00:02:42.820',
    },
    {
      duration: 0,
      content: 'And I give lots of evidence.',
      startOfParagraph: null,
      startTime: '00:02:45.179',
    },
    {
      duration: 0,
      content: 'Persist!',
      startOfParagraph: null,
      startTime: '00:02:47.111',
    },
    {
      duration: 0,
      content: 'Joe Kraus says,',
      startOfParagraph: null,
      startTime: '00:02:48.619',
    },
    {
      duration: 0,
      content: '"Persistence is the number one reason for our success."',
      startOfParagraph: null,
      startTime: '00:02:49.644',
    },
    {
      duration: 0,
      content:
        "You've got to persist through failure. You've got to persist through crap!",
      startOfParagraph: null,
      startTime: '00:02:52.652',
    },
    {
      duration: 0,
      content:
        'Which of course means "Criticism, Rejection, Assholes and Pressure."',
      startOfParagraph: null,
      startTime: '00:02:56.218',
    },
    {
      duration: 0,
      content: '(Laughter)',
      startOfParagraph: null,
      startTime: '00:02:59.757',
    },
    {
      duration: 0,
      content: 'So, the answer to this question is simple:',
      startOfParagraph: null,
      startTime: '00:03:02.547',
    },
    {
      duration: 0,
      content: 'Pay 4,000 bucks and come to TED.',
      startOfParagraph: null,
      startTime: '00:03:06.290',
    },
    {
      duration: 0,
      content: '(Laughter)',
      startOfParagraph: null,
      startTime: '00:03:08.439',
    },
    {
      duration: 0,
      content: 'Or failing that, do the eight things -- and trust me,',
      startOfParagraph: null,
      startTime: '00:03:09.656',
    },
    {
      duration: 0,
      content: 'these are the big eight things that lead to success.',
      startOfParagraph: null,
      startTime: '00:03:12.418',
    },
    {
      duration: 0,
      content: 'Thank you TEDsters for all your interviews!',
      startOfParagraph: null,
      startTime: '00:03:15.662',
    },
    {
      duration: 0,
      content: '(Applause)\n\n',
      startOfParagraph: null,
      startTime: '00:03:18.405',
    },
  ],
};
export const SUBTITLES_RU = {
  key: 'start',
  convert: false,
  textKey: 'text',
  data: [
    {
      start: 11.82,
      end: 14.82,
      text:
        'Вообще, это двухчасовая презентация, с которой я выступаю перед старшеклассниками',
    },
    {
      start: 14.82,
      end: 15.82,
      text: 'и которую я урезал до трёх минут.',
    },
    {
      start: 15.82,
      end: 18.82,
      text: 'Всё началось в самолете, когда я летел на конференцию TED,',
    },
    {
      start: 18.82,
      end: 19.82,
      text: 'семь лет назад.',
    },
    {
      start: 19.82,
      end: 20.82,
      text: 'Моей соседкой',
    },
    {
      start: 20.82,
      end: 24.82,
      text: 'была старшеклассница, подросток,',
    },
    {
      start: 24.82,
      end: 26.82,
      text: 'из очень бедной семьи.',
    },
    {
      start: 26.82,
      end: 29.82,
      text: 'Она хотела достичь чего-нибудь в жизни,',
    },
    {
      start: 29.82,
      end: 30.82,
      text: 'и задала мне простой вопрос.',
    },
    {
      start: 30.82,
      end: 32.82,
      text: 'Она спросила: «Что приводит к успеху?»',
    },
    {
      start: 32.82,
      end: 34.82,
      text: 'И я почувствовал себя очень неловко,',
    },
    {
      start: 34.82,
      end: 36.82,
      text: 'потому что не мог ей ответить.',
    },
    {
      start: 36.82,
      end: 39.82,
      text: 'Я сошел с самолета, приехал на конференцию TED,',
    },
    {
      start: 39.82,
      end: 42.82,
      text: 'И вдруг подумал — боже, вокруг меня столько успешных людей!',
    },
    {
      start: 42.82,
      end: 45.82,
      text: 'Почему бы не спросить их, что помогло им достичь успеха,',
    },
    {
      start: 45.82,
      end: 47.82,
      text: 'а потом рассказать об этом детям?',
    },
    {
      start: 47.82,
      end: 51.82,
      text: 'И вот мы здесь, через семь лет, с результатами 500 интервью,',
    },
    {
      start: 51.82,
      end: 54.82,
      text: 'и я собираюсь рассказать вам, что действительно ведет к успеху',
    },
    {
      start: 54.82,
      end: 56.82,
      text: 'и движет участниками нашего сообщества.',
    },
    {
      start: 56.82,
      end: 58.82,
      text: 'Первое — это страсть.',
    },
    {
      start: 58.82,
      end: 61.82,
      text: 'Фриман Томас сказал: «Мною движет страсть».',
    },
    {
      start: 61.82,
      end: 64.82,
      text: 'Участники TED делают это ради любви, не ради денег.',
    },
    {
      start: 64.82,
      end: 67.82,
      text:
        'Как говорит Кэрол Коллета: «Я бы платила кому-то, чтобы делать то, что делаю я».',
    },
    {
      start: 67.82,
      end: 69.82,
      text: 'Самое интересное во всем этом,',
    },
    {
      start: 69.82,
      end: 71.82,
      text:
        'что если вы делаете свое дело с любовью, деньги придут так или иначе.',
    },
    {
      start: 71.82,
      end: 75.82,
      text: 'Работа! Руперт Мердок сказал мне: «Это тяжелая работа.',
    },
    {
      start: 75.82,
      end: 77.82,
      text:
        'Ничего не приходит само собой. Но я получаю огромное удовольствие».',
    },
    {
      start: 77.82,
      end: 81.82,
      text: 'Это Руперт произнес «удовольствие»? Да!',
    },
    {
      start: 81.82,
      end: 85.82,
      text:
        'Участники TED получают удовольствие от работы. Они работают много и упорно.',
    },
    {
      start: 85.82,
      end: 88.82,
      text: 'Но я сделал для себя вывод — они не трудоголики. Они трудолюбы.',
    },
    {
      start: 89.82,
      end: 95.82,
      text:
        'Профессионализм! Алекс Гарден говорит: «Чтобы быть успешным, выберите что-нибудь одно',
    },
    {
      start: 95.82,
      end: 96.82,
      text: 'и станьте в этом самым лучшим».',
    },
    {
      start: 96.82,
      end: 99.82,
      text: 'Никаких чудес, практика, практика, практика.',
    },
    {
      start: 99.82,
      end: 102.82,
      text: 'Концентрация. Как сказал Норман Джуисон:',
    },
    {
      start: 102.82,
      end: 105.82,
      text:
        '«Я думаю, главное в том, чтобы сконцентрироваться на самом важном».',
    },
    {
      start: 105.82,
      end: 109.82,
      text: 'Преодоление! По мнению Дэвида Галло: «Нужно преодолевать себя.',
    },
    {
      start: 109.82,
      end: 111.82,
      text: 'Преодолевать физически, преодолевать духовно».',
    },
    {
      start: 111.82,
      end: 114.82,
      text: 'Вы должны преодолеть застенчивость и неуверенность.',
    },
    {
      start: 114.82,
      end: 117.82,
      text: 'Голди Хоун говорит: «Я всегда сомневалась в себе.',
    },
    {
      start: 117.82,
      end: 119.82,
      text: 'Я была недостаточно хороша, недостаточно умна.',
    },
    {
      start: 119.82,
      end: 121.82,
      text: 'Я и не думала, что у меня что-то получится».',
    },
    {
      start: 121.82,
      end: 123.82,
      text: 'Преодоление не дается нам легко,',
    },
    {
      start: 123.82,
      end: 128.82,
      text: 'вот поэтому и изобрели матерей. (Смех)',
    },
    {
      start: 128.82,
      end: 131.82,
      text: 'Фрэнк Гири сказал мне:',
    },
    {
      start: 131.82,
      end: 132.82,
      text: '«Меня толкала моя мать».',
    },
    {
      start: 133.82,
      end: 138.82,
      text: 'Служение! Шервин Нуланд: «Для меня было честью служить врачом».',
    },
    {
      start: 138.82,
      end: 141.82,
      text: 'Сейчас многие дети говорят, что хотели бы стать миллионерами.',
    },
    {
      start: 141.82,
      end: 142.82,
      text: 'Первое, что я им отвечаю:',
    },
    {
      start: 142.82,
      end: 144.82,
      text: '«Отлично, но вы не можете служить только себе,',
    },
    {
      start: 144.82,
      end: 146.82,
      text: 'вы должны отдавать что-то ценное другим.',
    },
    {
      start: 146.82,
      end: 149.82,
      text: 'Потому что только так люди становятся богатыми».',
    },
    {
      start: 150.82,
      end: 154.82,
      text:
        'Идеи. Один из участников TED, Билл Гейтс, говорит: «У меня была идея —',
    },
    {
      start: 154.82,
      end: 157.82,
      text:
        'сделать первую компанию по производству программ для микрокомпьютеров».',
    },
    {
      start: 157.82,
      end: 159.82,
      text: 'И я бы сказал, это была отличная идея.',
    },
    {
      start: 159.82,
      end: 162.82,
      text: 'Нет никакой магии в том, как к нам приходят идеи,',
    },
    {
      start: 162.82,
      end: 164.82,
      text: 'надо всего лишь делать самые простые вещи.',
    },
    {
      start: 164.82,
      end: 166.82,
      text: 'Я доказываю это на многочисленных примерах.',
    },
    {
      start: 166.82,
      end: 168.82,
      text: 'Настойчивость. Джо Краус говорит:',
    },
    {
      start: 168.82,
      end: 171.82,
      text: '«Главная причина нашего успеха — настойчивость».',
    },
    {
      start: 171.82,
      end: 175.82,
      text:
        'Вы должны пробиваться через неудачи, пробиваться через CRAP! [англ. «дерьмо»].',
    },
    {
      start: 175.82,
      end: 178.82,
      text: 'Конечно, это означает — «Критика, Отторжение, Дураки и Давление».',
    },
    {
      start: 178.82,
      end: 180.82,
      text: '(Смех)',
    },
    {
      start: 181.82,
      end: 185.82,
      text: 'Итак, главный ответ на наш вопрос прост:',
    },
    {
      start: 185.82,
      end: 188.82,
      text: 'Заплатите 4000 долларов и приезжайте на TED.',
    },
    {
      start: 188.82,
      end: 191.82,
      text: 'Если это не подходит, следуйте восьми приведенным принципам',
    },
    {
      start: 191.82,
      end: 195.82,
      text: 'и, поверьте мне, это то, что ведет к успеху.',
    },
    {
      start: 195.82,
      end: 197.82,
      text: 'И спасибо всем участникам TED за ваши интервью!',
    },
  ],
};
