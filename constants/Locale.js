import {
  RU,
  ABOUT,
  NAV,
  ALBUM,
  ALBUM_CNT,
  PLAYLIST,
  SHORT_TEXT,
  CATALOG,
  FAVORITES,
} from '.';
import {getObject} from '../components/helpers';

const LOCALES = {
  [RU]: {
    [ABOUT]: {
      title: 'Разработчик приложение',
      call2action: 'Сервисы для связи с разработчиком \n(нажмите на иконку)',
    },
    [FAVORITES]: {
      title: 'Избранные',
    },
    [NAV]: {
      home: 'Лента',
      about: 'Открыть доступ',
      catalog: 'Каталог',
      favorites: 'Избранные',
    },
    [ALBUM]: {
      count: 'видео',
    },
    [ALBUM_CNT]: {
      showAll: 'Смотреть все',
    },
    [PLAYLIST]: {
      video: 'видео',
    },
    [SHORT_TEXT]: {
      showAll: 'Еще',
      hide: 'Скрыть',
    },
    [CATALOG]: {
      all: 'Каталог',
      showAll: 'Смотреть все',
    },
  },
};

export default function getLocale(lang = RU, page) {
  return getObject(getObject(LOCALES[lang])[page]);
}
