export const API_URL = 'https://big-holding.kz/video-app/public';
export const YANDEX_API = 'https://translate.yandex.net/api/v1.5/tr.json/';
export const YANDEX_KEY =
  'trnsl.1.1.20200430T134437Z.dd7a44671af69d0c.cb1468e69b3c6885235b0bdd7b57ddaaa20b868f';

export const APP_ID = 2;

export const TYPES = {
  playlist: 1,
  video: 2,
  slide: 3,
};

export const CATALOG = 'type:catalog';
// export const VIDEO_TYPE = 2;

export const FINAL = 2000;

export const POSITIVE = 'type:positive';
export const POSITIVE_DOC = 'type:positiveDoctor';
export const NEGATIVE = 'type:negative';
export const NEGATIVE_ILL = 'type:negativeIll';

export const POSITIVE_DOC_COMP = 'type:positiveDoctorComplicated';

export const RU = 'lang:ru';
export const KZ = 'lang:kz';
export const EN = 'lang:en';

export const NAV = 'screen:navigation';

export const ABOUT = 'screen:about';
export const HOME = 'screen:home';
export const FAVORITES = 'screen:favorites';
export const SURVEY = 'screen:survey';

export const FAVORITES_STORAGE = 'FAVORITES';
export const ADD_STORAGE = 'ADD';
export const YTUBE_STORAGE = 'YTUBE_STORAGE';
export const DEVICE_STORAGE = 'DEVICE_INFO';

export const YOUTUBE_API_KEY = 'AIzaSyBcbIVXJ6EWucAqsnSeUx3ko6nZGmlNmMM';

export const ALBUM = 'component:album';
export const ALBUM_CNT = 'component:album-container';
export const PLAYLIST = 'PLAYLIST';
export const PROMO = 'PROMO';
export const VIDEO = 'VIDEO';
export const SHORT_TEXT = 'component:shortText';

export const SEARCH = 'component:search';

//Carousel types
export const STACK = 'stack';
export const DEFAULT = 'default';
