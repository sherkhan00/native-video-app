import React, {useEffect, useState} from 'react';

import Main from '../components/Main';
import {useLang} from '../components/hooks';
import getLocale from '../constants/Locale';
import {HOME, TYPES, STACK, DEFAULT} from '../constants/';
import SliderCnt from '../components/home/SliderCnt';
import MainApi from '../api/MainApi';
import Promos from '../components/home/Promos';
import {getObject} from '../components/helpers';
import CarouselR from '../components/home/CarouselR';
// import useYTubeStatus from '../components/hooks/useYtubeStatus';

export default function HomeScreen({navigation}) {
  const lang = useLang();
  const strings = getLocale(lang, HOME);
  const [app, setApp] = useState({promos: []});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    MainApi.getHomeInfo().then((res) => {
      setApp(res);
      setLoading(false);
    });
  }, []);

  function goto(params) {
    navigation.navigate('Player');
  }

  function getHomePromos(arr) {
    return arr.filter((i) => i.type_id !== TYPES.slide);
  }

  return (
    <Main loading={loading}>
      <CarouselR
        data={
          getObject(app.promos.filter((i) => i.type_id === TYPES.slide)[0])
            .elements
        }
        type={DEFAULT}
      />
      {/* <SliderCnt
        data={getObject(app.promos.filter((i) => i.type_id === TYPES.slide)[0])}
      /> */}
      {/* <Button onPress={goto}>
        <Text> Go to video</Text>
      </Button> */}

      {getHomePromos(app.promos).map((i) => (
        <Promos key={i.id} {...i} />
      ))}
    </Main>
  );
}

HomeScreen.navigationOptions = {
  header: null,
};
