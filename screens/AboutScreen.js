import React, {useEffect, useState} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  TouchableHighlight,
  Linking,
  AsyncStorage,
} from 'react-native';
import Main from '../components/Main';
import getLocale from '../constants/Locale';
import {RU, ABOUT} from '../constants';
import {useLang} from '../components/hooks';
import MainApi from '../api/MainApi';
import Payment from '../components/payment';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Button from '../components/Form/Button';
import Colors from '../constants/Colors';

export default function AboutScreen({navigation}) {
  const lang = useLang();
  const strings = getLocale(lang, ABOUT);

  const [app, setApp] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    MainApi.getHomeInfo().then((res) => {
      setApp(res);
      setLoading(false);
    });
  }, []);

  return (
    <Main loading={loading}>
      <Payment />
      {/* <Text style={styles.title}>{strings.title}</Text>
      <View style={styles.img}>
        <Image style={{}} source={require('../assets/images/avatar.png')} />
      </View>
      <Text style={styles.desc}>{app.about}</Text>*/}
      <Text style={styles.call2action}>{strings.call2action}</Text>

      <View style={styles.img2}>
        <Button
          style={{backgroundColor: '#da2251', ...styles.btn}}
          onPress={() =>
            Linking.openURL('instagram://user?username=sherkhan.baibatyr')
          }>
          <Ionicons name="logo-instagram" size={50} color={Colors.white} />
        </Button>
        <Button
          style={{backgroundColor: '#2ec042', ...styles.btn}}
          onPress={() =>
            Linking.openURL('https://api.whatsapp.com/send?phone=77474323750')
          }>
          <Ionicons name="logo-whatsapp" size={50} color={Colors.white} />
        </Button>
        <Button
          style={{backgroundColor: '#4861a3', ...styles.btn}}
          onPress={() =>
            Linking.openURL('https://web.facebook.com/sherkhan.baibatyr')
          }>
          <Ionicons name="logo-facebook" size={50} color={Colors.white} />
        </Button>
      </View>
    </Main>
  );
}

AboutScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  title: {
    // fontFamily: "Roboto-medium",
    fontWeight: '500',
    fontSize: 18,
  },
  call2action: {
    // fontFamily: 'Roboto-medium',
    fontWeight: '500',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 10,
    lineHeight: 25,
  },
  small: {
    // fontFamily: 'Roboto',
    fontSize: 10,
  },
  desc: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    marginVertical: 10,
    lineHeight: 20,
  },
  img: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  img2: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    marginVertical: 20,
  },
  btn: {
    width: 85,
    height: 85,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: Colors.yellow,
    borderRadius: 100,
  },
});
