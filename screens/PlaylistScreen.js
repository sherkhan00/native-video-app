import React, {useState, useEffect} from 'react';
import {useLang} from '../components/hooks';
import getLocale from '../constants/Locale';
import {HOME, VIDEO, PLAYLIST, TYPES} from '../constants';
import ScrollList from '../components/ScrollList';

export default function PlaylistScreen({route, navigation}) {
  const lang = useLang();
  const strings = getLocale(lang, PLAYLIST);
  const {id, title, description} = route.params;

  return (
    <ScrollList
      title={title}
      description={description}
      type={TYPES.playlist}
      id={id}
    />
  );
}

PlaylistScreen.navigationOptions = {
  header: null,
};
