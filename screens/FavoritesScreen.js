import React, {useState, useEffect} from 'react';
import Main from '../components/Main';
import {useLang} from '../components/hooks';
import Album from '../components/home/Album';
import {TYPES, FAVORITES_STORAGE, FAVORITES} from '../constants';
import getLocale from '../constants/Locale';
import useFavorites from '../components/hooks/useFavorites';
import {View, AsyncStorage} from 'react-native';
import {useIsFocused} from '@react-navigation/native';

export default function FavoritesScreen({route, navigation}) {
  const lang = useLang();
  const strings = getLocale(lang, FAVORITES);
  const [favorites, setFavorites] = useState([]);
  const [loading, setLoading] = useState(false);
  const isFocused = useIsFocused();

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      _retrieveData();
    }, 1000);
  }, [isFocused]);

  async function _retrieveData() {
    try {
      const fav = await AsyncStorage.getItem(FAVORITES_STORAGE);
      if (fav !== null) {
        setFavorites(JSON.parse(fav));
      }
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  }

  // console.log("favorites", favorites);

  return (
    <Main title={strings.title} loading={loading}>
      <View style={{marginTop: 20}}>
        {favorites.map((i) => (
          <Album key={i.id} type={TYPES.video} {...i} />
        ))}
      </View>
    </Main>
  );
}

FavoritesScreen.navigationOptions = {
  header: null,
};
