import React, {useState, useEffect} from 'react';
import {View, ScrollView, StyleSheet, Text} from 'react-native';
import Main from '../components/Main';
import {useLang} from '../components/hooks';
import MainApi from '../api/MainApi';
import {tempStyles} from '../styles';
import Album from '../components/home/Album';
import {PROMO, PLAYLIST, TYPES} from '../constants';
import {getArray, getObject} from '../components/helpers';

export default function ShowAllScreen({route, navigation}) {
  const lang = useLang();
  const [data, setData] = useState({elements: []});
  const [loading, setLoading] = useState(true);
  const {id, type = PROMO} = route.params;

  useEffect(() => {
    switch (type) {
      case PROMO:
        MainApi.getPromoInfo(id).then((res) => {
          setData(res);
          setLoading(false);
        });
        break;
      case PLAYLIST:
        MainApi.getPlaylistInfo(id).then((res) => {
          setData(res);
          setLoading(false);
        });
        break;

      default:
        break;
    }
  }, []);

  let key = type === PROMO ? 'name' : 'title',
    key2 = type === PROMO ? 'elements' : 'videos',
    typeId = type === PROMO ? getObject(data.type).id : TYPES.video;

  return (
    <Main
      title={data[key]}
      loading={loading}
      showClose={true}
      onClose={() => navigation.goBack()}>
      {getArray(data[key2]).map((i) => (
        <Album key={i.id} type={typeId} {...i} />
      ))}
    </Main>
  );
}

ShowAllScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  title: {
    // fontFamily: "Roboto-medium",
    fontWeight: '500',
    fontSize: 18,
    marginVertical: 10,
  },
  desc: {
    lineHeight: 20,
    // fontFamily: 'Roboto',
    fontSize: 14,
    marginVertical: 5,
    paddingRight: 10,
  },
});
