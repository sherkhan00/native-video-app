/* eslint-disable no-useless-escape */
import React, {useState, useEffect} from 'react';
import {useLang} from '../components/hooks';
import {VIDEO, EN, RU} from '../constants';
import {View, StyleSheet, Text} from 'react-native';
import MainApi from '../api/MainApi';
import {converToJson_SRT, getObject} from '../components/helpers/';
import Spinner from '../components/Form/Spinner';
import Colors from '../constants/Colors';
import useAdd from '../components/hooks/useAdd';
import MyVideoPlayer from '../components/Player/MyVideoPlayer';
import SubtitleView from '../components/Player/Subtitle';
import VideoInfo from '../components/Player/VideoInfo';
import useDevice from '../components/hooks/useDevice';
import DialogR from '../components/Form/DialogR';
import SubtitlesBtn from '../components/Player/Subtitle/SubtitlesBtn';
import ActivateAccount from '../components/Player/ActivateAccount';

var intervalHandle = null,
  paramsObj = {};
const INTERVAL = 500;

export default function PlayerScreen({route, navigation}) {
  const lang = useLang();
  const [device, update] = useDevice();
  const [dialogVisible, setDialogVisible] = useState(false);
  const [videoVisible, setVideoVisible] = useState(true);

  const [data, setData] = useState({});
  const [time, setTime] = useState(0);

  const [subtitleEn, setSubtitleEn] = useState([]);
  const [subtitleRu, setSubtitleRu] = useState([]);

  const [showSubtitle, setShowSubtitle] = useState({
    [EN]: true,
    [RU]: true,
  });
  const [fullView, setFullView] = useState(false);

  const setVideoShowCount = useAdd();

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const videoId = getObject(route.params).id;
    paramsObj = {};

    if (device.id) {
      if (!device.isActive) setVideoShowCount();

      MainApi.getVideoInfo(videoId, device.id).then((res) => {
        setData(res);
        MainApi.getSubtitle(res.subtitle_en).then((subtitle) => {
          if (subtitle) {
            let arr = converToJson_SRT(subtitle);
            MainApi.getSubtitle(res.subtitle_ru).then((subtitle2) => {
              if (subtitle2) {
                let arr2 = converToJson_SRT(subtitle2);
                setSubtitleRu(arr2);
              }
            });
            setSubtitleEn(arr);
          }
          setLoading(false);
          startCountDown();
        });
      });
    }
  }, [device]);

  function onProgress(params) {
    if (!device.isActive && parseFloat(params.currentTime) > 600) {
      setVideoVisible(false);
    } else paramsObj = params;
    // if (!params.isBuffering && params.shouldPlay) {
    //   paramsObj = params;
    // }
  }

  function tick() {
    setTime(paramsObj);
  }

  function startCountDown() {
    intervalHandle = setInterval(tick, INTERVAL);
  }

  function onPressSubtitlesBtn(type) {
    let obj = {...showSubtitle};
    obj[type] = !obj[type];
    setShowSubtitle(obj);
  }

  function onMorePress() {
    setDialogVisible(true);
  }

  if (loading) return <Spinner />;

  return (
    <View style={{backgroundColor: Colors.white}}>
      <View style={styles.backGrey}></View>
      <MyVideoPlayer
        onProgress={onProgress}
        video={data}
        handleFullView={(s) => setFullView(s)}
        onMorePress={onMorePress}
      />
      {!videoVisible && <ActivateAccount />}

      <SubtitleView
        fullView={fullView}
        show={showSubtitle}
        onPressSubtitlesBtn={onPressSubtitlesBtn}
        subtitlesEn={subtitleEn}
        subtitlesRu={subtitleRu}
        time={time}
        shortVideo={data.short}
      />
      <VideoInfo data={data} />
      <DialogR
        visible={dialogVisible}
        setVisible={setDialogVisible}
        content={
          <SubtitlesBtn show={showSubtitle} onPress={onPressSubtitlesBtn} />
        }
      />
    </View>
  );
}

PlayerScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  closeIcon: {
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginRight: 20,
  },
  backGrey: {
    backgroundColor: Colors.grey,
    width: '40%',
    position: 'absolute',
    top: 0,
    //left: 0,
    bottom: 0,
    right: 0,
    zIndex: 0,
  },
  contentContainer: {
    height: '100%',
    marginHorizontal: 15,
    paddingTop: 10,
    paddingBottom: 30,
    justifyContent: 'flex-start',
    flexGrow: 1,
    //flex: 3,
    zIndex: 10,
  },
});
