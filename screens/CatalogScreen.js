import React, {useState, useEffect} from 'react';
import {useLang} from '../components/hooks';
import getLocale from '../constants/Locale';
import {CATALOG, TYPES, PLAYLIST, SEARCH} from '../constants';

import ScrollList from '../components/ScrollList';
import MainApi from '../api/MainApi';
import Promos from '../components/home/Promos';
import Spinner from '../components/Form/Spinner';
import {View, Text} from 'react-native';
import {tempStyles} from '../styles';
import Button from '../components/Form/Button';
import CarouselR from '../components/home/CarouselR';
import Main from '../components/Main';
import Search from '../components/Form/Search';

export default function CatalogScreen({route, navigation}) {
  const lang = useLang();
  const strings = getLocale(lang, CATALOG);
  const [loading, setLoading] = useState(true);
  const [searchTxt, setSearchTxt] = useState('');
  const [list, setList] = useState([]);

  function showAll(id) {
    navigation.navigate('ShowAll', {id: id, type: PLAYLIST});
  }

  useEffect(() => {
    MainApi.getPlaylists().then((res) => {
      setList(res.playlists);
      setLoading(false);
    });
  }, []);

  function handleSearch(value) {
    setSearchTxt(value);
  }

  return (
    <Main loading={loading} scroll={searchTxt ? false : true}>
      <Search searchTxt={searchTxt} handleSearch={handleSearch} />
      {searchTxt ? (
        <ScrollList type={SEARCH} searchTxt={searchTxt} includeMain={false} />
      ) : (
        <>
          {list.map(({id, title, elements}) => (
            <>
              <View
                style={{
                  marginVertical: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Text style={tempStyles.title}>{title}</Text>
                <Button style={tempStyles.linkBtn} onPress={() => showAll(id)}>
                  <Text style={tempStyles.greyTxt}>{strings.showAll} </Text>
                </Button>
              </View>
              <CarouselR data={elements} />
            </>
          ))}
        </>
      )}
    </Main>
  );
  // return (
  //   <ScrollList
  //     showClose={false}
  //     title={strings.all}
  //     type={CATALOG}
  //     renderType={TYPES.playlist}
  //   />
  // );
}

CatalogScreen.navigationOptions = {
  header: null,
};
