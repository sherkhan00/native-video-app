import instance from '.';
import {APP_ID, YANDEX_API, YOUTUBE_API_KEY} from '../constants';

class MainApi {
  static getHomeInfo() {
    return instance(false)
      .get(`/apps/${APP_ID}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  //-----------  Playlist -------
  static getPlaylists(page) {
    return instance(false)
      .get(`apps/${APP_ID}/playlists?page=${page}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  static getPlaylistInfo(id) {
    return instance(false)
      .get(`/playlists/${id}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  static getPlaylistVideo(id, page) {
    return instance(false)
      .get(`/playlists/${id}/videos?page=${page}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  //-----------  Promo -------
  static getPromoInfo(id) {
    return instance(false)
      .get(`/promos/${id}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  //-----------  Video -------
  static getVideoInfo(id, deviceId) {
    return instance(false)
      .get(`/videos/${id}/${deviceId}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  static getTranslate(text) {
    return instance(false)
      .get(`/translate/${text}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  static getSubtitle(path) {
    return instance(false)
      .get('/subtitles/' + path)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  static activateApp(token, deviceId) {
    return instance(false)
      .get(`/activate/${token}/${deviceId}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  static searchVideo(search) {
    return instance(false)
      .get(`/search/${search}/${APP_ID}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }

  static checkDevice(deviceId) {
    return instance(false)
      .get(`/check-device/${deviceId}`)
      .then((response) => response.data)
      .catch((err) => {
        console.log('err', err);
      });
  }
}

export default MainApi;
