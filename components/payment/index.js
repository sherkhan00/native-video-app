import React, {useState} from 'react';
import {StyleSheet, View, Text, TextInput, Alert} from 'react-native';
import Button from '../Form/Button';
import Colors from '../../constants/Colors';
import MainApi from '../../api/MainApi';
import useDevice from '../hooks/useDevice';
import Spinner from '../Form/Spinner';
import {setDevice} from '../helpers';

export default function Payment() {
  const [value, onChangeText] = React.useState('');
  const [loading, setLoading] = useState(false);
  const [device, update] = useDevice();

  function onPressSubmit() {
    if (!value) Alert.alert('Ошибка', 'Заполните код активации');
    else {
      setLoading(true);
      MainApi.activateApp(device.id, value).then((res) => {
        if (res.success) {
          console.log('*** activateApp', res);
          setDevice(res.data);
          update();
          // Alert.alert('Успех', res.message);
        } else {
          Alert.alert('Ошибка', res.message);
        }
        setLoading(false);
      });
    }
  }

  if (device.isActive)
    return <Text style={styles.success}>PRO версия активирован!</Text>;

  return (
    <View style={styles.container}>
      {loading ? (
        <Spinner />
      ) : (
        <>
          <Text style={styles.welcome}>Активация PRO версии</Text>
          <Text style={styles.welcome2}>
            Чтобы активировать введите код активации полученный от разработчика
          </Text>
          <TextInput
            placeholder="Код активации"
            style={styles.textInput}
            onChangeText={(text) => onChangeText(text)}
            value={value}
          />
          <View>
            <Button onPress={onPressSubmit} style={styles.button}>
              <Text style={styles.btnTxt}>Активировать</Text>
            </Button>
          </View>
          <Text style={styles.bottomTxt}>
            Чтобы получить код свяжитесь с разработчиком
          </Text>
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    marginVertical: 20,
    justifyContent: 'center',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: Colors.greyInWhite,
    paddingVertical: 10,
    minHeight: 200,
  },
  button: {
    width: 250,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: '#007BFF',
    backgroundColor: '#007BFF',
    padding: 15,
    margin: 5,
  },
  welcome: {
    fontSize: 18,
    textAlign: 'center',
    margin: 10,
  },
  welcome2: {
    fontSize: 14,
    textAlign: 'center',
    margin: 10,
  },
  bottomTxt: {
    fontSize: 14,
    color: Colors.orange,
    textAlign: 'center',
    margin: 10,
  },
  success: {
    fontSize: 18,
    color: Colors.white,
    backgroundColor: Colors.green,
    textAlign: 'center',
    margin: 10,
    borderRadius: 30,
    // borderWidth: 1,
    height: 50,
    paddingTop: 11,
  },
  textInput: {
    width: 250,
    textAlign: 'center',
    // borderColor: '#CCCCCC',
    borderWidth: 1,
    borderRadius: 30,
    borderBottomWidth: 1,
    height: 50,
    fontSize: 14,
  },
  btnTxt: {
    textAlign: 'center',
  },
});
