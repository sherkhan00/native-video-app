import React, { useState, useEffect } from "react";
import { Text, View } from "react-native";
import { getShortTxt } from "./helpers";
import Button from "./Form/Button";
import { tempStyles } from "../styles";
import { useLang } from "./hooks";
import getLocale from "../constants/Locale";
import { SHORT_TEXT } from "../constants/";

export default function ShortText({ text = "", separate = 80 }) {
  const lang = useLang();
  const strings = getLocale(lang, SHORT_TEXT);

  const [txt, setTxt] = useState("");

  useEffect(() => {
    setTxt(getShortTxt(text, separate));
  }, [text]);

  function showAllTxt() {
    if (txt === text) {
      setTxt(getShortTxt(text, separate));
      return;
    }
    setTxt(text);
  }

  return (
    <View onPress={showAllTxt}>
      <Text>{txt}</Text>
      {text ? (
        <Button style={tempStyles.linkBtn} onPress={showAllTxt}>
          <Text style={tempStyles.greyTxt}>
            {txt !== text ? strings.showAll : strings.hide}
          </Text>
        </Button>
      ) : null}
    </View>
  );
}
