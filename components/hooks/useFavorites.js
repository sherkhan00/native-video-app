import {AsyncStorage, Dimensions} from 'react-native';
import {useState, useEffect} from 'react';
import _ from 'lodash';
import {FAVORITES_STORAGE} from '../../constants';

export default function useFavorites() {
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    _retrieveData();
  }, []);

  async function _retrieveData() {
    try {
      const fav = await AsyncStorage.getItem(FAVORITES_STORAGE);
      if (fav !== null) {
        setFavorites(JSON.parse(fav));
      }
    } catch (error) {
      console.error(error);
    }
  }

  async function setFavorite(fav = {}) {
    try {
      let isExist = favorites.filter((i) => i.id === fav.id);

      let newFavs = [...favorites];
      if (isExist.length > 0) {
        _.remove(newFavs, function (i) {
          return i.id === fav.id;
        });
      } else {
        newFavs.push(fav);
      }
      await AsyncStorage.setItem(FAVORITES_STORAGE, JSON.stringify(newFavs));
      _retrieveData();
    } catch (error) {
      console.error(error);
    }
  }

  return [favorites, setFavorite];
}
