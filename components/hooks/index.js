import {RU} from '../../constants';
import {AsyncStorage, Dimensions} from 'react-native';
import {useState, useEffect} from 'react';
import _ from 'lodash';

export function useLang() {
  const [language, setLanguage] = useState(RU);

  useEffect(() => {
    _retrieveData();
  }, []);

  async function _retrieveData() {
    try {
      const lang = await AsyncStorage.getItem('LANG');
      if (lang !== null) {
        setLanguage(lang);
      }
    } catch (error) {
      console.error(error);
    }
  }

  return language;
}

export const getWidth = () => Math.round(Dimensions.get('window').width);

export function useCurrentWitdh() {
  // save current window width in the state object
  let [width, setWidth] = useState(getWidth());

  return width;
}

export const getHeight = () => Math.round(Dimensions.get('window').height);

export function useCurrentHeight() {
  // save current window width in the state object
  let [height, setHeight] = useState(getHeight());

  return height;
}
