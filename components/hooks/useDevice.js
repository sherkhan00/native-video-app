import {AsyncStorage, Dimensions} from 'react-native';
import {useState, useEffect} from 'react';
import {DEVICE_STORAGE} from '../../constants';
import {getObject, isEmpty} from '../helpers';

export default function useDevice() {
  const [device, setDevice] = useState({});
  const [value, setValue] = useState(0);

  useEffect(() => {
    _retrieveData();
  }, []);

  async function _retrieveData() {
    try {
      const dev = await AsyncStorage.getItem(DEVICE_STORAGE);
      if (dev !== null) {
        setDevice(JSON.parse(dev));
      }
    } catch (error) {
      console.error(error);
    }
  }

  async function update() {
    _retrieveData();
    setValue((value) => ++value);
  }

  return [device, update];
}
