import {AsyncStorage} from 'react-native';
import {useState, useEffect} from 'react';
import {YTUBE_STORAGE} from '../../constants';

export default function useYTubeStatus() {
  const [isInstalled, setIsInstalled] = useState(0);

  useEffect(() => {
    _retrieveData();
  }, []);

  async function _retrieveData() {
    try {
      const ytStatus = await AsyncStorage.getItem(YTUBE_STORAGE);
      if (ytStatus !== null) {
        setIsInstalled(parseInt(ytStatus));
      }
    } catch (error) {
      console.error(error);
    }
  }

  async function setYTubeStatus(status) {
    try {
      await AsyncStorage.setItem(YTUBE_STORAGE, status);
    } catch (error) {
      console.error(error);
    }
  }

  return [isInstalled, setYTubeStatus];
}
