import {AsyncStorage, Dimensions} from 'react-native';
import {useState, useEffect} from 'react';
import {ADD_STORAGE} from '../../constants';
import showInterstitial from '../add/showInterstitial';

export default function useAdd() {
  // const [showCount, setShowCount] = useState(0);

  // useEffect(() => {
  //   _retrieveData();
  // }, []);

  async function setVideoShowCount() {
    try {
      const countS = await AsyncStorage.getItem(ADD_STORAGE);
      let count = parseInt(countS) + 1;
      // console.log('count', count, countS);
      if (count > 1) {
        showInterstitial();
        await AsyncStorage.setItem(ADD_STORAGE, '0');
      } else if (count === 1) {
        await AsyncStorage.setItem(ADD_STORAGE, count + '');
      } else {
        await AsyncStorage.setItem(ADD_STORAGE, '0');
      }
    } catch (error) {
      console.error(error);
    }
  }

  return setVideoShowCount;
}
