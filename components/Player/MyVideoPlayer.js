import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TouchableHighlight, Text} from 'react-native';
import {getWidth, getHeight, useCurrentWitdh, useCurrentHeight} from '../hooks';
import Colors from '../../constants/Colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';
import Video from 'react-native-af-video-player';

function MyVideoPlayer({video, onProgress, handleFullView, onMorePress}) {
  const [fullView, setFullView] = useState(false);

  const navigation = useNavigation();

  useEffect(() => {
    return () => {
      // Orientation.lockToPortrait();
    };
  }, []);

  function onFullScreen(status) {
    handleFullView(status);
    setFullView(status);
  }

  return (
    <View style={{height: fullView ? 'auto' : 250, ...styles.container}}>
      {fullView || (
        <View style={{marginVertical: 10}}>
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor="#DDDDDD"
            style={styles.closeIcon}
            onPress={() => navigation.goBack()}>
            <Ionicons name="ios-close-circle-outline" size={30} />
          </TouchableHighlight>
        </View>
      )}
      <Video
        title={video.title}
        style={styles.video}
        url={video.url}
        rotateToFullScreen={true}
        onFullScreen={onFullScreen}
        onMorePress={onMorePress}
        onProgress={onProgress}
      />
    </View>
  );
}

export default React.memo(MyVideoPlayer, (prevProps, nextProps) => true);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    // backgroundColor: Colors.black,
    // height: '100%',
  },
  overlay: {
    ...StyleSheet.absoluteFill,
  },
  overlaySet: {
    flexDirection: 'row',
  },
  video: {
    height: 200,
  },
  head: {
    backgroundColor: Colors.black,
    height: getWidth() / 1.4,
    position: 'absolute',
    top: 0,
    width: '100%',
  },
  closeIcon: {
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginRight: 20,
  },
});
