import React, {useEffect} from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import Colors from '../../../constants/Colors';
import Word from './Word';
import {EN, RU} from '../../../constants';

export default function SubtitleOnFull({
  show = {},
  subtitleEn = {},
  subtitleRu = {},
}) {
  let words = [];
  if (subtitleEn.text) words = subtitleEn.text.split(' ');

  return (
    <View style={styles.container}>
      {show[EN] && (
        <View style={styles.wordView}>
          {words.map((i, index) => (
            <Word key={index} text={i}></Word>
          ))}
        </View>
      )}
      {show[RU] && <Text style={styles.word}>{subtitleRu.text}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    position: 'absolute',
    top: 270,
    //padding: 20,
    // backgroundColor: Colors.orange,
    zIndex: 1000,
  },
  subtitleView: {
    position: 'absolute',
    bottom: 40,
    // backgroundColor: Colors.orange,
  },
  wordView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    padding: 3,
  },
  word: {
    fontSize: 14,
    color: Colors.orange,
    backgroundColor: Colors.black,
    textAlign: 'center',
  },
});
