import React, {useState, useEffect} from 'react';
import SubtitleOnFull from './SubtitleOnFull';
import SubtitleOnPortrait from './SubtitleOnPortrait';
import moment from 'moment';
import {EN, RU} from '../../../constants';
import {getObject} from '../../helpers';

export default function SubtitleView({
  fullView = false,
  show = {},
  subtitlesEn = [],
  subtitlesRu = [],
  time,
  onPressSubtitlesBtn,
  shortVideo = false,
}) {
  let subtitleEn = {},
    subtitleRu = {};

  onProgress(time);

  function onProgress(params) {
    let currentSecond = parseFloat(params.currentTime),
      duration = parseInt(params.seekableDuration);

    if (show[EN]) {
      subtitleEn = getObject(findSub(currentSecond, duration, true));
    }
    // RU
    if (show[RU]) {
      subtitleRu = getObject(findSub(currentSecond, duration, false));
    }
  }

  function findSub(currentSecond, duration, isEn = true) {
    let sub = {},
      percent = (currentSecond * 100) / duration,
      start = 0,
      end = 1000,
      list = [];
    if (isEn) {
      list = subtitlesEn;
    } else {
      list = subtitlesRu;
    }
    if (shortVideo) {
      end = list.length - 1;
    } else {
      start = parseInt(list.length * (parseInt(percent - 15) / 100));
      end = parseInt(list.length * (parseInt(percent + 15) / 100));
      if (start < 0) start = 0;
      if (end > list.length - 1) end = list.length - 1;
    }
    try {
      for (let i = start; i < end; i++) {
        let item = list[i];
        if (
          isBiggest(
            currentSecond,
            item.start.substring(0, 8),
            item.end.substring(0, 8),
          )
        ) {
          sub = item;
          break;
        }
      }
    } catch (error) {
      console.log('--------------- error', error, start, end);
    }
    return sub;
  }

  function isBiggest(currentSecond, start, end) {
    let startSec = null,
      endSec = null;
    startSec = moment.duration(start, 'HH:mm:ss').asSeconds();
    endSec = moment.duration(end, 'HH:mm:ss').asSeconds();

    return currentSecond > startSec && currentSecond < endSec;
  }

  return (
    <>
      {fullView ? (
        <SubtitleOnFull
          show={show}
          subtitleEn={subtitleEn}
          subtitleRu={subtitleRu}
          onPressSubtitlesBtn={onPressSubtitlesBtn}
        />
      ) : (
        <SubtitleOnPortrait
          show={show}
          subtitleEn={subtitleEn}
          subtitleRu={subtitleRu}
          onPressSubtitlesBtn={onPressSubtitlesBtn}
        />
      )}
    </>
  );
}
