import React, {useEffect} from 'react';
import {
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import Colors from '../../../constants/Colors';
import MainApi from '../../../api/MainApi';

export default function Word({text}) {
  const [visible, setVisible] = React.useState(false);
  const [translate, setTranslate] = React.useState('');

  useEffect(() => {
    setTranslate('');
  }, [text]);

  function togglePopover(params) {
    setVisible(!visible);
  }

  function getTranslate(params) {
    MainApi.getTranslate(text).then((res) => {
      setTranslate(res.text.join(', '));
      // setVisible(true);
      // setLoading(false);
    });
  }

  return (
    <>
      <TouchableHighlight
        activeOpacity={0.6}
        underlayColor="#DDDDDD"
        style={styles.wordBtn}
        onPress={getTranslate}>
        <Text style={styles.word}>
          {text}
          {translate ? ` [${translate}]` : ''}
        </Text>
      </TouchableHighlight>
    </>
  );
}

function Popover({children}) {
  return <>{children}</>;
}

const styles = StyleSheet.create({
  wordBtn: {
    paddingHorizontal: 4,
    justifyContent: 'center',
    backgroundColor: Colors.black,
  },
  word: {
    fontSize: 18,
    color: Colors.white,
    // textDecorationLine: 'underline',
    // textDecorationStyle: 'double',
  },
  popover: {
    padding: 10,
    borderRadius: 2,
  },
});
