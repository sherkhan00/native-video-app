import React from 'react';
import Button from '../../../components/Form/Button';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../../../constants/Colors';
import {EN, RU} from '../../../constants';
import {ToggleR} from '../../Form/ToggleR';

export default function SubtitlesBtn({show = {}, onPress}) {
  function getBtnStyle(type) {
    return show[type] ? styles.btnFilled : styles.btn;
  }

  function getTxtStyle(type) {
    return show[type] ? styles.textFilled : styles.text;
  }

  // return (
  //   <View style={{flexDirection: 'row', width: '15%'}}>
  //     <Button onPress={() => onPress(EN)} style={getBtnStyle(EN)}>
  //       <Text style={getTxtStyle(EN)}>EN</Text>
  //     </Button>
  //     <Button onPress={() => onPress(RU)} style={getBtnStyle(RU)}>
  //       <Text style={getTxtStyle(RU)}>RU</Text>
  //     </Button>
  //   </View>
  // );

  return (
    <View style={{}}>
      <ToggleR
        label={`Английские субтитры`}
        checked={show[EN]}
        setChecked={() => onPress(EN)}
      />
      <ToggleR
        label={`Русские субтитры`}
        checked={show[RU]}
        setChecked={() => onPress(RU)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  btn: {
    marginHorizontal: 10,
    borderRadius: 50,
    borderColor: Colors.white,
    borderWidth: 2,
    padding: 5,
  },
  btnFilled: {
    marginHorizontal: 10,
    borderRadius: 50,
    borderColor: Colors.white,
    backgroundColor: Colors.white,
    borderWidth: 2,
    padding: 5,
  },
  text: {
    fontSize: 10,
    color: Colors.white,
  },
  textFilled: {
    fontSize: 10,
    color: Colors.black,
  },
});
