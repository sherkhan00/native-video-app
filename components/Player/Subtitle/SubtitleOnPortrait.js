import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {EN, RU} from '../../../constants';
import Word from './Word';
import Colors from '../../../constants/Colors';

export default function SubtitleOnPortrait({
  show = {},
  subtitleEn = {},
  subtitleRu = {},
}) {
  let words = [];
  if (subtitleEn.text) words = subtitleEn.text.split(' ');

  return (
    <View style={styles.container}>
      {show[EN] && (
        <View style={styles.wordView}>
          {words.map((i, index) => (
            <Word key={index} text={i}></Word>
          ))}
        </View>
      )}
      {show[RU] && <Text style={styles.word}>{subtitleRu.text}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    minHeight: 50,
  },
  wordView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    padding: 3,
  },
  word: {
    fontSize: 14,
    textAlign: 'center',
    color: Colors.orange,
  },
});
