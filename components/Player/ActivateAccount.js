import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Colors from '../../constants/Colors';
import Icon from 'react-native-vector-icons/Ionicons';
export default function ActivateAccount() {
  return (
    <View style={styles.container}>
      <View style={styles.left}>
        <Icon name="ios-information-circle" size={50} style={styles.icon} />
      </View>
      <View style={styles.right}>
        <Text style={styles.text}>
          В пробной версии можно смотреть только первые 10 минут
        </Text>
        <Text style={styles.text}>
          Чтобы продолжить активируйте PRO версию!
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 50,
    width: '100%',
    height: 250,
    backgroundColor: Colors.red,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: 1200,
  },
  text: {
    fontSize: 16,
    color: Colors.white,
    lineHeight: 20,
  },
  contentContainer: {
    height: '100%',
    marginHorizontal: 15,
    paddingTop: 10,
    paddingBottom: 30,
    justifyContent: 'flex-start',
    flexGrow: 1,
    //flex: 3,
    zIndex: 10,
  },
  icon: {
    color: Colors.white,
  },
  left: {
    width: '15%',
  },
  right: {
    width: '80%',
  },
});
