import React from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
} from 'react-native';
import TextIcon from '../text';
import ShortText from '../ShortText';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Colors from '../../constants/Colors';
import {tempStyles} from '../../styles';
import useFavorites from '../hooks/useFavorites';

function VideoInfo({data}) {
  const [favorites, setFavorite] = useFavorites();
  function onPressFavorite() {
    setFavorite(data);
  }

  function isFavorite() {
    let isExist = favorites.filter((i) => i.id === data.id);
    if (isExist.length > 0) return true;
    return false;
  }

  return (
    <ScrollView contentContainerStyle={styles.contentContainer}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View style={{width: '80%'}}>
          <Text style={tempStyles.bigTitle}>{data.title}</Text>
          <TextIcon
            text={data.record}
            icon="ios-timer"
            style={{marginVertical: 10, ...tempStyles.title}}
          />
        </View>
        <View>
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor="#DDDDDD"
            style={{borderRadius: 100, justifyContent: 'center'}}
            onPress={onPressFavorite}>
            <Ionicons
              style={{color: Colors.orange}}
              name={isFavorite() ? 'ios-heart' : 'ios-heart-empty'}
              size={30}
            />
          </TouchableHighlight>
        </View>
      </View>

      <ShortText text={data.description} textStyle={tempStyles.desc} />
    </ScrollView>
  );
}

export default React.memo(VideoInfo, (prevProps, nextProps) => true);
const styles = StyleSheet.create({
  closeIcon: {
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginRight: 20,
  },
  backGrey: {
    backgroundColor: Colors.grey,
    width: '40%',
    position: 'absolute',
    top: 0,
    //left: 0,
    bottom: 0,
    right: 0,
    zIndex: 0,
  },
  contentContainer: {
    height: '100%',
    marginHorizontal: 15,
    paddingTop: 10,
    paddingBottom: 30,
    justifyContent: 'flex-start',
    flexGrow: 1,
    //flex: 3,
    zIndex: 10,
  },
});
