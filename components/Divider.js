import React from 'react';
import {View} from 'react-native';
import Colors from '../constants/Colors';

export default function Divider() {
  return (
    <View
      style={{
        marginVertical: 20,
        marginHorizontal: 10,
        height: 1,
        backgroundColor: Colors.greyInGrey,
        opacity: 0.4,
      }}></View>
  );
}
