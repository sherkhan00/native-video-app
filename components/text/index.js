import React from 'react';
import {Text, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function TextIcon({
  text = '',
  icon = 'ios',
  size = 15,
  style = {},
}) {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
      }}>
      <Ionicons name={icon} size={size} style={{padding: 2, ...style}} />
      <Text style={style}>{text}</Text>
    </View>
  );
}
