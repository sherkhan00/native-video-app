import React, {useState, useEffect} from 'react';
import {View, FlatList, ActivityIndicator, Text} from 'react-native';
import {TYPES, CATALOG, SEARCH} from '../constants';
import Album from './home/Album';
import Main from './Main';
import {useNavigation} from '@react-navigation/native';
import MainApi from '../api/MainApi';
import {tempStyles} from '../styles';
import {useCurrentHeight} from './hooks';
import ShortText from './ShortText';
import Spinner from './Form/Spinner';

function ScrollList({
  title,
  description,
  id,
  searchTxt = '',
  type,
  renderType = TYPES.video,
  showClose = true,
  includeMain = true,
}) {
  const [list, setList] = useState([]);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [loadingMore, setLoadingMore] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [count, setCount] = useState([]);
  const navigation = useNavigation();

  useEffect(() => {
    getList();
  }, [page]);

  function getList() {
    switch (type) {
      case TYPES.playlist:
        MainApi.getPlaylistVideo(id, page).then((res) => {
          let data = res.data;
          if (refreshing || page === 1) setList(data);
          else setList([...list, ...data]);

          setCount(res.total);
          setLoading(false);
          setRefreshing(false);
          setLoadingMore(false);
        });
        break;
      case CATALOG:
        MainApi.getPlaylists(page).then((res) => {
          let data = res.data;
          if (refreshing || page === 1) setList(data);
          else setList([...list, ...data]);

          setCount(res.total);
          setLoading(false);
          setRefreshing(false);
          setLoadingMore(false);
        });
        break;
      case SEARCH:
        MainApi.searchVideo(searchTxt).then((res) => {
          console.log('searchVideo', res);
          let data = res.data;
          if (refreshing || page === 1) setList(data);
          else setList([...list, ...data]);

          setCount(res.total);
          setLoading(false);
          setRefreshing(false);
          setLoadingMore(false);
        });
        break;

      default:
        break;
    }
  }

  function handleLoadMore() {
    if (loadingMore || refreshing) return;
    if (list.length < count) {
      setPage(page + 1);
      setLoadingMore(true);
    }
  }

  function handleRefresh() {
    if (refreshing) return;
    setRefreshing(true);
    if (page === 1) {
      getList();
      return;
    }
    setPage(1);
  }

  function renderFooter() {
    if (!loadingMore) return <View style={{height: 150}}></View>;

    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          height: 150,
          marginTop: 10,
          marginBottom: 30,
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  }

  function renderHeader() {
    return (
      <View>
        <ShortText text={description} textStyle={tempStyles.description} />
      </View>
    );
  }

  function RenderScroll() {
    return (
      <View>
        {loading ? (
          <Spinner />
        ) : (
          <FlatList
            ListHeaderComponent={renderHeader}
            style={{paddingBottom: 20}}
            data={list}
            keyExtractor={(item, i) => i.toString()}
            renderItem={({item}) => (
              <Album
                key={item.id}
                type={renderType}
                onPress={() => onPressVideo(item)}
                {...item}
              />
            )}
            onEndReached={handleLoadMore}
            initialNumToRender={20}
            ListFooterComponent={renderFooter}
            onRefresh={handleRefresh}
            refreshing={refreshing}
          />
        )}
      </View>
    );
  }

  return (
    <>
      {includeMain ? (
        <Main
          loading={loading}
          showClose={showClose}
          onClose={() => navigation.goBack()}
          title={title}
          scroll={false}>
          <RenderScroll />
        </Main>
      ) : (
        <RenderScroll />
      )}
    </>
  );
}
export default React.memo(ScrollList);
