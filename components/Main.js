import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  TouchableHighlight,
  Text,
} from 'react-native';
import Colors from '../constants/Colors';
import {tempStyles} from '../styles';
import AddContainer from './add';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Spinner from './Form/Spinner';

export default function Main({
  children,
  showClose = false,
  onClose,
  loading = false,
  scroll = true,
  title = '',
}) {
  function showHeader() {
    if (title || showClose) return true;
    else return false;
  }

  return (
    <View style={styles.container}>
      <View style={styles.backGrey}></View>
      <View style={styles.scrollView}>
        {showHeader() && (
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Text style={{width: '80%', ...tempStyles.screenTitle}}>
              {title}
            </Text>
            {showClose && (
              <TouchableHighlight
                activeOpacity={0.6}
                underlayColor="#DDDDDD"
                style={{...styles.closeIcon}}
                onPress={onClose}>
                <Ionicons name="ios-close-circle-outline" size={30} />
              </TouchableHighlight>
            )}
          </View>
        )}
        {loading ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%',
              height: '100%',
            }}>
            <Spinner size="giant" />
          </View>
        ) : (
          <>
            {scroll ? (
              <ScrollView contentContainerStyle={styles.contentContainer}>
                {children}
                <View style={{height: 100}}></View>
              </ScrollView>
            ) : (
              <View style={styles.contentContainer}>{children}</View>
            )}
          </>
        )}
      </View>
      <AddContainer />
    </View>
  );
}

{
  /* <ScrollView contentContainerStyle={styles.contentContainer}>
            {children}
          </ScrollView> */
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // justifyContent: "center",
    flexDirection: 'column',
  },
  scrollView: {
    marginTop: 5,
  },
  contentContainer: {
    marginHorizontal: 15,
    paddingTop: 10,
    paddingBottom: 30,
    justifyContent: 'center',
    flexGrow: 1,
    zIndex: 10,
  },
  closeIcon: {
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginRight: 20,
  },
  backGrey: {
    backgroundColor: Colors.grey,
    width: '40%',
    position: 'absolute',
    top: 0,
    //left: 0,
    bottom: 0,
    right: 0,
    zIndex: 0,
  },
});
