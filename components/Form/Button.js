import React from 'react';
import {TouchableHighlight} from 'react-native';
import Colors from '../../constants/Colors';

export default function Button({style = {}, onPress, children}) {
  return (
    <TouchableHighlight
      underlayColor={Colors.grey}
      style={style}
      onPress={onPress}>
      {children}
    </TouchableHighlight>
  );
}
