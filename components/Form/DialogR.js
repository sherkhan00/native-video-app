import React, {useState, useEffect} from 'react';
import Dialog, {DialogContent} from 'react-native-popup-dialog';

export default function DialogR({visible = false, setVisible, content}) {
  return (
    <Dialog
      visible={visible}
      onTouchOutside={() => {
        setVisible(false);
      }}>
      <DialogContent>{content}</DialogContent>
    </Dialog>
  );
}
