import React from 'react';
import {View, StyleSheet, Text, Image, ActivityIndicator} from 'react-native';
import Colors from '../../constants/Colors';

export default function Spinner() {
  return (
    <View style={{...StyleSheet.absoluteFillObject, justifyContent: 'center'}}>
      <ActivityIndicator
        size="large"
        color={Colors.tabIconSelected}
        style={{alignSelf: 'center'}}
      />
    </View>
  );
}
