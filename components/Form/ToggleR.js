import React from 'react';
import {Switch, Text, View} from 'react-native';
import Colors from '../../constants/Colors';

export const ToggleR = ({label, checked, setChecked}) => {
  const onCheckedChange = () => {
    setChecked(!checked);
  };

  return (
    <View
      style={{flexDirection: 'row', alignItems: 'center', marginVertical: 10}}>
      <Switch
        trackColor={{false: '#767577', true: Colors.blue}}
        thumbColor={checked ? Colors.white : '#f4f3f4'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={onCheckedChange}
        value={checked}
      />
      <Text>{label}</Text>
    </View>
  );
};
