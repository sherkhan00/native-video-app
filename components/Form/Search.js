import React, {useState, useEffect} from 'react';
import {SearchBar} from 'react-native-elements';

var inputTimer = null,
  duration = 1000;
export default function Search({searchTxt, handleSearch}) {
  const [search, setSearch] = useState('');

  useEffect(() => {
    setSearch(searchTxt);
  }, [searchTxt]);

  function updateSearch(value) {
    console.log('updateSearch', value);
    setSearch(value);

    clearTimeout(inputTimer);
    inputTimer = setTimeout(() => {
      handleSearch(value);
    }, duration);
  }

  return (
    <SearchBar
      placeholder="Поиск..."
      onChangeText={updateSearch}
      value={search}
      containerStyle={{
        backgroundColor: 'transparent',
        borderWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
      }}
    />
  );
}
