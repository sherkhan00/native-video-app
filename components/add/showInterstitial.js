import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';

function showInterstitial() {
  console.log('----- showInterstitial called');
  const BANNER_ID = 'ca-app-pub-6422123332940210/3895201457',
    TEST_BANNER = 'ca-app-pub-3940256099942544/1033173712';

  AdMobInterstitial.setAdUnitID(BANNER_ID);
  AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
  AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
  return;
}

export default showInterstitial;
