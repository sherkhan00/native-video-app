import React, {useEffect, useState} from 'react';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';
import {View} from 'react-native';
import Colors from '../../constants/Colors';
import useDevice from '../hooks/useDevice';
import {getDevice} from '../helpers';

export default function AddContainer() {
  const [device, setDeviceInfo] = useDevice();
  const [showAdd, setShowAdd] = useState(true);

  useEffect(() => {
    handleShow();
  }, [device]);

  async function handleShow(params) {
    let dev = await getDevice();
    console.log('AddContainer', dev);
    if (dev.isActive) setShowAdd(false);
    else setShowAdd(true);
  }
  // handleShow();

  if (showAdd) {
    const BANNER_ID = 'ca-app-pub-6422123332940210/6458603859',
      TEST_BANNER = 'ca-app-pub-3940256099942544/6300978111';
    return (
      <View
        style={{
          width: '100%',
          backgroundColor: Colors.white,
          position: 'absolute',
          bottom: 0,
          zIndex: 200,
        }}>
        <AdMobBanner
          adSize="fullBanner"
          adUnitID={BANNER_ID}
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={(error) => console.warn(error)}
        />
      </View>
    );
  } else {
    return <></>;
  }
}
