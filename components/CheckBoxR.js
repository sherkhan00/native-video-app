import React from 'react';
import {View, Text, StyleSheet, CheckBox} from 'react-native';

export default function CheckBoxR({data = {}, checked = false, onChange}) {
  return (
    <View style={styles.block}>
      <CheckBox text={data.text} checked={checked} onChange={onChange} />
      {data.desc && <Text style={styles.info}>{data.desc}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  block: {
    marginVertical: 10,
  },
  info: {
    // fontFamily: "Roboto",
    fontSize: 10,
    marginLeft: 30,
  },
});
