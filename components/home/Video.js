import React from 'react';
import {View, Image, Text} from 'react-native';
import {useLang} from '../hooks';
import getLocale from '../../constants/Locale';
import {ALBUM} from '../../constants';
import Divider from '../Divider';
import Colors from '../../constants/Colors';
import TextIcon from '../text';
import {getShortTxt} from '../helpers';

export default function Video({imgUrl, title, description, record, styles}) {
  const lang = useLang();
  const strings = getLocale(lang, ALBUM);

  return (
    <>
      <View style={styles.imgBlock}>
        <Image style={styles.img} source={{uri: imgUrl}} />
      </View>
      <View style={styles.textBlock}>
        <Text style={styles.title}>{getShortTxt(title, 50)}</Text>
        <Text style={styles.desc}>{getShortTxt(description, 100)}</Text>
        <TextIcon text={record} icon="ios-timer" style={styles.count} />
      </View>
    </>
  );
}
