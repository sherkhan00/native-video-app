import React from "react";
import { View, Image, Text } from "react-native";
import getLocale from "../../constants/Locale";
import { useLang } from "../hooks";
import { ALBUM } from "../../constants";
import TextIcon from "../text";
import { getShortTxt } from "../helpers";

export default function Playlist({ imgUrl, title, video_count = 0, styles }) {
  const lang = useLang();
  const strings = getLocale(lang, ALBUM);

  return (
    <>
      <View style={styles.imgBlock}>
        <Image style={styles.img} source={{ uri: imgUrl }} />
      </View>
      <View style={styles.textBlock}>
        <Text style={styles.title}>{getShortTxt(title, 50)}</Text>
        <TextIcon
          text={`${video_count ? video_count : 0} ${strings.count}`}
          icon="ios-play-circle"
          style={styles.count}
        />
      </View>
    </>
  );
}
