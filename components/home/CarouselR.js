import React, {useState, useRef} from 'react';
import Carousel from 'react-native-snap-carousel';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableHighlight,
} from 'react-native';
import Colors from '../../constants/Colors';
import {TYPES, DEFAULT, STACK} from '../../constants';
import {useNavigation} from '@react-navigation/native';

const test = [
  {
    title: 'Item 1',
    text: 'Text 1',
  },
  {
    title: 'Item 2',
    text: 'Text 2',
  },
  {
    title: 'Item 3',
    text: 'Text 3',
  },
  {
    title: 'Item 4',
    text: 'Text 4',
  },
  {
    title: 'Item 5',
    text: 'Text 5',
  },
];
export default function CarouselR({data = test, type = STACK}) {
  const [activeIndex, setActiveIndex] = useState(0);
  const carousel = useRef(null);
  const navigation = useNavigation();

  function onPressAlbum(id) {
    navigation.navigate('Player', {id: id});
    // let {type, id} = data;
    // switch (type) {
    //   case TYPES.playlist:
    //     navigation.navigate('Playlist', {id: id, ...data});
    //     break;
    //   case TYPES.video:
    //     navigation.navigate('Video', {id: id});
    //     break;
    //   default:
    //     break;
    // }
  }

  function renderItem({item, index}) {
    return (
      <TouchableHighlight
        underlayColor={Colors.grey}
        style={styles.block}
        onPress={() => onPressAlbum(item.id)}>
        <Image
          style={{
            width: '100%',
            height: '100%',
            borderRadius: 5,
          }}
          source={{uri: item.imgUrl}}
        />
      </TouchableHighlight>
    );
  }

  return (
    <SafeAreaView
      style={{
        flex: 1,
        paddingTop: 10,
        paddingBottom: 10,
      }}>
      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
        <Carousel
          firstItem={type === STACK ? data.length - 1 : 1}
          layout={type} //stack default
          ref={carousel}
          data={data}
          sliderWidth={330}
          itemWidth={200}
          renderItem={renderItem}
          onSnapToItem={(index) => setActiveIndex(index)}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  block: {
    width: '100%',
    backgroundColor: 'floralwhite',
    borderRadius: 5,
    height: 250,
    marginLeft: 10,
    marginRight: 10,
  },
  slide: {},
  title: {
    fontFamily: 'Din-medium',
    color: Colors.white,
    fontSize: 20,
    zIndex: 100,
    lineHeight: 30,
  },
  img: {
    height: 370,
    width: '91%',
    alignSelf: 'flex-start',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 30,
  },
  overImg: {
    position: 'absolute',
    alignSelf: 'flex-start',
    width: '92%',
    zIndex: 100,
    height: 250,
    padding: 20,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 25,
    backgroundColor: 'rgba(4,5,13,0.6)',
  },
});
