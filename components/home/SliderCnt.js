import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import Slider from './Slider';
import Colors from '../../constants/Colors';
import _ from 'lodash';
import {useCurrentWitdh} from '../hooks';
import {getObject, getArray} from '../helpers';
import {TYPES} from '../../constants';
import {useNavigation} from '@react-navigation/native';

export default function SliderCnt({data = {}}) {
  const width = useCurrentWitdh();
  let images = _.map(data.elements, 'imgUrl');

  const navigation = useNavigation();

  function renderSlideImage(params = {}) {
    let uri = getObject(params.source).uri;
    let currentImg = getObject(
      getArray(data.elements).filter((i) => i.url === uri)[0],
    );

    return (
      <View>
        {currentImg.showText && (
          <View style={styles.overImg}>
            <Text style={styles.title}>{currentImg.title}</Text>
          </View>
        )}
        <Image {...params} />
      </View>
    );
  }

  function onPressImage(index) {
    let el = data.elements[index];
    navigation.navigate('Playlist', {...el});
  }

  return (
    <View style={styles.block}>
      <Slider
        parentWidth={width}
        imgStyle={styles.img}
        images={images}
        renderSlideImage={renderSlideImage}
        onPressImage={onPressImage}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  block: {},
  title: {
    fontFamily: 'Din-medium',
    color: Colors.white,
    fontSize: 20,
    zIndex: 100,
    lineHeight: 30,
  },
  img: {
    height: 370,
    width: '91%',
    alignSelf: 'flex-start',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 30,
  },
  overImg: {
    position: 'absolute',
    alignSelf: 'flex-start',
    width: '92%',
    zIndex: 100,
    height: 250,
    padding: 20,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 25,
    backgroundColor: 'rgba(4,5,13,0.6)',
  },
});
