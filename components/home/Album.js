import React from 'react';
import {View, Image, Text, StyleSheet, TouchableHighlight} from 'react-native';
import Colors from '../../constants/Colors';
import getLocale from '../../constants/Locale';
import {useLang} from '../hooks';
import {ALBUM, PLAYLIST, VIDEO, TYPES} from '../../constants';
import {SHADOW} from '../../styles';
import Playlist from './Playlist';
import Video from './Video';
import {useNavigation} from '@react-navigation/native';

export default function Album({id, type, ...props}) {
  const lang = useLang();
  const strings = getLocale(lang, ALBUM);
  const navigation = useNavigation();

  function onPressAlbum() {
    navigation.navigate('Player', {id: id});
    // switch (type) {
    //   case TYPES.playlist:
    //     navigation.navigate('Playlist', {id: id, ...props});
    //     break;
    //   case TYPES.video:
    //     navigation.navigate('Video', {id: id});
    //     break;
    //   default:
    //     break;
    // }
  }

  return (
    <TouchableHighlight
      underlayColor={Colors.grey}
      style={styles.block}
      onPress={onPressAlbum}>
      <>
        {type === TYPES.playlist && <Playlist styles={styles} {...props} />}
        {type === TYPES.video && <Video styles={styles} {...props} />}
      </>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  block: {
    flexDirection: 'row',
    borderBottomColor: 'rgba(0, 0, 0, .1)',
    borderBottomWidth: 1,
    width: '100%',
    paddingVertical: 10,
  },
  title: {
    // fontFamily: 'Roboto-medium',
    fontWeight: '600',
    color: Colors.txtClr,
    fontSize: 14,
    lineHeight: 18,
  },
  desc: {
    fontWeight: '400',
    color: Colors.txtClr,
    fontSize: 12,
    lineHeight: 16,
  },
  count: {
    // fontFamily: 'Roboto-medium',
    fontWeight: '600',
    color: Colors.greyInWhite,
    fontSize: 12,
    marginBottom: 5,
  },
  img: {
    width: '100%',
    // margin: 5,
    height: 150,
    borderRadius: 2,
  },
  imgBlock: {
    width: '40%',
    ...SHADOW,
  },
  textBlock: {
    width: '50%',
    justifyContent: 'space-between',
  },
  overImg: {
    position: 'absolute',
    alignSelf: 'flex-start',
    width: '90%',
    zIndex: 100,
    height: 250,
    padding: 20,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 25,
    backgroundColor: 'rgba(4,5,13,0.6)',
  },
});
