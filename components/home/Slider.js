import React, {useState} from 'react';
import {SliderBox} from 'react-native-image-slider-box';
import Colors from '../../constants/Colors';
import {Image, View, Text} from 'react-native';

export default function Slider({
  images = [],
  data = [],
  imgStyle = {},
  styles,
  renderSlideImage,
  parentWidth,
  onPressImage,
}) {
  return (
    <SliderBox
      loop={true}
      autoplay={true}
      parentWidth={parentWidth}
      ImageComponent={renderSlideImage}
      images={images}
      dotColor={Colors.yellow}
      inactiveDotColor={Colors.grey}
      ImageComponentStyle={imgStyle}
      BlockStyle={{
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 30,
      }}
      onCurrentImagePressed={onPressImage}
      // currentImageEmitter={(index) => setPosition(index)}
    />
  );
}
