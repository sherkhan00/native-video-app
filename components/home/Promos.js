import React from 'react';
import Album from './Album';
import {View, FlatList, Text} from 'react-native';
import {tempStyles} from '../../styles';
import {useLang} from '../hooks';
import getLocale from '../../constants/Locale';
import {ALBUM_CNT, PLAYLIST, VIDEO, TYPES} from '../../constants';
import Button from '../Form/Button';
import Colors from '../../constants/Colors';
import Divider from '../Divider';
import {useNavigation} from '@react-navigation/native';
import CarouselR from './CarouselR';

export default function Promos({id, name = '', type = {}, elements = []}) {
  const lang = useLang();
  const strings = getLocale(lang, ALBUM_CNT);
  const navigation = useNavigation();

  function showAll() {
    navigation.navigate('ShowAll', {id: id});
  }

  return (
    <View>
      <View
        style={{
          marginVertical: 10,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <Text style={tempStyles.title}>{name}</Text>
        <Button style={tempStyles.linkBtn} onPress={showAll}>
          <Text style={tempStyles.greyTxt}>{strings.showAll} </Text>
        </Button>
      </View>
      <CarouselR data={elements} />
      {/* {elements.map((i) => (
        
        // <Album key={i.id} type={type.id} {...i} />
      ))} */}
      {/* <Divider /> */}
    </View>
  );
}
