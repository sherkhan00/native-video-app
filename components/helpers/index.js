import {AsyncStorage} from 'react-native';
import {DEVICE_STORAGE} from '../../constants';

export const getShortTxt = (str = '', length = 80) => {
  let string = '';
  if (!str) return '';
  if (str.length > length) {
    string = str.slice(0, length) + ' ...';
  } else {
    string = str;
  }
  return string;
};

export const getArray = (arr = []) => {
  if (arr.length > 0) {
    return arr;
  }
  return [];
};

export const getObject = (obj = {}) => {
  if (obj) {
    return obj;
  }
  return {};
};

export const replace = (str) => {
  return str.replace("'", "'");
};

export function converToJson_SRT(data) {
  let data2 = data.replace(/&gt;/g, '>');

  let srt = data2.replace(/\r\n|\r|\n/g, '\n');
  srt = strip(srt);
  var srt_ = srt.split('\n\n');
  var cont = 0;
  var subtitles = [];

  for (let s in srt_) {
    let st = srt_[s].split('\n');
    if (st.length >= 2) {
      var st2 = st[1].split(' --> ');
      var t = st[2];

      if (st.length > 2) {
        for (let j = 3; j < st.length; j++) t += '\n' + st[j];
      }

      subtitles[cont] = {
        number: st[0],
        start: st2[0],
        end: st2[1],
        text: t
          ? t.replace(/(<([^>]+)>)/gi, '').replace(/(\r\n|\n|\r)/gm, ' ')
          : '',
      };
      cont++;
    }
  }
  return subtitles;
}

function strip(s) {
  return s.replace(/^\s+|\s+$/g, '');
}

export async function getDevice() {
  try {
    const dev = await AsyncStorage.getItem(DEVICE_STORAGE);
    if (dev !== null) return JSON.parse(dev);
    return {};
  } catch (error) {
    console.error(error);
  }
}

export async function setDevice(info = {}, createNewId = false) {
  try {
    console.log('--------------- setDeviceInfo call ', info, createNewId);
    if (createNewId) {
      console.log('--------------- createNewId ');
      let id = Math.random().toString(36).substring(2, 15);
      await AsyncStorage.setItem(
        DEVICE_STORAGE,
        JSON.stringify({id, isActive: false}),
      );
    } else if (!isEmpty(info) && !createNewId) {
      console.log('--------------- Change old state ');
      await AsyncStorage.setItem(DEVICE_STORAGE, JSON.stringify(info));
    }
  } catch (error) {
    console.error(error);
  }
}

export function isEmpty(obj) {
  // null and undefined are "empty"
  if (obj == null) return true;

  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;

  // If it isn't an object at this point
  // it is empty, but it can't be anything *but* empty
  // Is it empty?  Depends on your application.
  if (typeof obj !== 'object') return true;

  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) return false;
  }

  return true;
}
