import * as React from 'react';
import {
  Platform,
  StatusBar,
  StyleSheet,
  View,
  AsyncStorage,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import BottomTabNavigator from './navigation/BottomTabNavigator';
import {RU, DEVICE_STORAGE} from './constants';
import PlayerScreen from './screens/PlayerScreen';
import MainApi from './api/MainApi';
import {getDevice, setDevice} from './components/helpers';
import Spinner from './components/Form/Spinner';

const Stack = createStackNavigator();

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const containerRef = React.useRef();

  React.useEffect(() => {
    _storeData();
    console.disableYellowBox = true;
    _retrieveData();
  }, []);

  async function _retrieveData() {
    try {
      const dev = await getDevice();
      console.log('***App***/// dev', dev);
      if (dev.id) {
        MainApi.checkDevice(dev.id).then((res) => {
          console.log('***App***/// res', res);
          if (res) setDevice(res);
          setLoadingComplete(true);
        });
      } else {
        console.log('***App***/// setDeviceInfo');
        setDevice(null, true);
        setLoadingComplete(true);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async function _storeData() {
    try {
      await AsyncStorage.setItem('LANG', RU);
    } catch (error) {
      console.log('error', error);
    }
  }

  if (!isLoadingComplete) return <Spinner />;

  return (
    <View style={styles.container}>
      {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
      <NavigationContainer
        ref={containerRef}
        initialState={initialNavigationState}>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="Root" component={BottomTabNavigator} />
          <Stack.Screen name="Player" component={PlayerScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
  // if (!isLoadingComplete && !props.skipLoadingScreen) {
  //   return null;
  // } else {

  // }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
