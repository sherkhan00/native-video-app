import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import * as React from 'react';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import {createStackNavigator} from '@react-navigation/stack';
import ShowAllScreen from '../screens/ShowAllScreen';
import AboutScreen from '../screens/AboutScreen';
import {NAV} from '../constants';
import getLocale from '../constants/Locale';
import {useLang} from '../components/hooks';
import PlaylistScreen from '../screens/PlaylistScreen';
import CatalogScreen from '../screens/CatalogScreen';
import FavoritesScreen from '../screens/FavoritesScreen';
import PlayerScreen from '../screens/PlayerScreen';

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';

const HomeStack = createStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator headerMode="none" tabBarVisible={false}>
      <HomeStack.Screen name="Home" component={HomeScreen} />
      <HomeStack.Screen name="ShowAll" component={ShowAllScreen} />
      <HomeStack.Screen name="Playlist" component={PlaylistScreen} />
    </HomeStack.Navigator>
  );
}

function CatalogStackScreen() {
  return (
    <HomeStack.Navigator headerMode="none">
      <HomeStack.Screen name="Catalog" component={CatalogScreen} />
      <HomeStack.Screen name="ShowAll" component={ShowAllScreen} />
      <HomeStack.Screen name="Playlist" component={PlaylistScreen} />
    </HomeStack.Navigator>
  );
}

function FavoritesStackScreen() {
  return (
    <HomeStack.Navigator headerMode="none">
      <HomeStack.Screen name="Favorites" component={FavoritesScreen} />
    </HomeStack.Navigator>
  );
}

export default function BottomTabNavigator({navigation, route}) {
  const lang = useLang();
  const strings = getLocale(lang, NAV);

  return (
    <BottomTab.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      headerMode="none">
      <BottomTab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          title: strings.home,
          tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused} name="ios-home" />
          ),
        }}
      />
      <BottomTab.Screen
        name="Catalog"
        component={CatalogStackScreen}
        options={{
          title: strings.catalog,
          tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused} name="ios-search" />
          ),
        }}
      />
      <BottomTab.Screen
        name="Favorites"
        component={FavoritesStackScreen}
        options={{
          title: strings.favorites,
          tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused} name="ios-heart-empty" />
          ),
        }}
      />
      <BottomTab.Screen
        name="About"
        component={AboutScreen}
        options={{
          title: strings.about,
          tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused} name="ios-unlock" />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName =
    route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {
    case 'Home':
      return 'Covid-19 скринер';
    case 'Links':
      return 'Links to learn more';
  }
}
