import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from '../constants/Colors';

export const SHADOW = {
  borderWidth: 0,
  borderRadius: 10,
  borderColor: '#fff',
  shadowColor: '#000',
  shadowOffset: {width: 0, height: 2},
  shadowOpacity: 0.7,
  shadowRadius: 10,
  elevation: 5,
  marginLeft: 5,
  padding: 3,
  marginRight: 5,
};

export const tempStyles = StyleSheet.create({
  screenTitle: {
    // fontFamily: "Roboto-medium",
    fontWeight: '600',
    color: Colors.txtClr,
    fontSize: 16,
    lineHeight: 20,
    marginLeft: 15,
  },
  bigTitle: {
    // fontFamily: "Roboto-medium",
    fontWeight: '600',
    color: Colors.txtClr,
    fontSize: 18,
    lineHeight: 22,
  },
  title: {
    // fontFamily: "Roboto-medium",
    fontWeight: '600',
    color: Colors.txtClr,
    fontSize: 14,
    lineHeight: 20,
  },
  warn: {
    // fontFamily: "Roboto",
    color: Colors.txtClr,
    fontSize: 12,
    lineHeight: 16,
  },
  greyTxt: {
    // fontFamily: "Roboto-medium",
    fontWeight: '500',
    color: Colors.greyInGrey,
    fontSize: 12,
  },
  linkBtn: {
    padding: 10,
  },
});
